/**
 * Класс описывающий котиков
 */
public class Cat {
    /**
     * кличка
     */
    private String nickname;
    /**
     * пол
     */
    private String gender;
    /**
     * год рождения
     */
    private int year;

    public String getGender() {
        return gender;
    }

    public int getYear() {
        return year;
    }

    public Cat(String nickname, String gender, int year) {
        this.nickname = nickname;
        this.gender = gender;
        this.year = year;
    }
}